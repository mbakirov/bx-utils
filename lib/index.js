var BX = require('./coa');

exports.version = require('../package.json').version;

exports.api = BX.api;
exports.util = require('./util');

exports.require = require;
