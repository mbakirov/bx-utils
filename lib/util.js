/* global toString: false */
'use strict';

var Q = require('q'),
    QFS = require('q-io/fs'),
    PATH = require('./path'),
    FS = require('fs'),
    UTIL = require('util'),
    MKDIRP = require('mkdirp');


/**
 * Create symbolic link.
 *
 * If `force` is specified and is true, it will check
 * for `link` to exist and remove it in case it is
 * a symbolic link.
 *
 * Files and directories will be left untouched.
 *
 * @param {String} link  Symbolic link name.
 * @param {String} target  Symbolic link target.
 * @param {Boolean} [force]  Force creating symplink in case it is already exist.
 * @return {Promise * Undefined}
 */
exports.symbolicLink = function(link, target, force) {

    return Q.resolve(force)
        .then(function(force) {

            if (!force) return;

            return QFS.statLink(link)
                .then(function(stat) {
                    if (stat.isSymbolicLink()) {
                        return QFS.remove(link);
                    }
                })
                .fail(function() {});

        })
        .then(function() {
            // TODO: pass correct type based on target? (Windows)
            return QFS.symbolicLink(link, target, 'file');
        });

};

/**
 * Remove path (file or directory) but not recursively.
 *
 * @param {String} path  Path to remove
 * @return {Promise * Undefined}
 */
exports.removePath = function(path) {

    return QFS.stat(path)
        .then(function(stat) {

            if (stat.isDirectory()) return QFS.removeDirectory(path);
            return QFS.remove(path);

        });

};

exports.write = function(path, content) {
    FS.writeFileSync(path, Array.isArray(content) ? content.join('') : content);
};

exports.writeFile = function(path, content) {
    return Q.when(content, function(content) {
        return QFS.write(path, Array.isArray(content) ? content.join('') : content, { charset: 'utf8' });
    });
};

exports.writeFileIfDiffers = function(path, content) {
    return QFS.exists(path)
        .then(function(exists) {
            if (!exists) return true;
            return exports.readFile(path)
                .then(function(current) {
                    return current !== content;
                });
        })
        .then(function(rewrite) {
            if (rewrite) return exports.writeFile(path, content);
        });
};

exports.readFile = function(path) {
    return QFS.read(path, { charset: 'utf8' });
};

exports.readBinary = function(path) {
    return QFS.read(path, { charset: 'binary' });
};

exports.mkdir = function(path) {
    try {
        FS.mkdirSync(path, '0777');
    } catch(ignore) {}
};

/**
 * Create directories.
 *
 * @return {String}  First directory being created.
 */
exports.mkdirs = MKDIRP.sync;

/**
 * Create directories.
 *
 * @return {Promise * String}  First directory being created.
 */
exports.mkdirp = Q.nfbind(MKDIRP);

exports.isExists = function(path) {
    var d = Q.defer();
    PATH.exists(path, function(res) {
        d.resolve(res);
    });
    return d.promise;
};

exports.isFile = function(path) {
    try {
        return FS.statSync(path).isFile();
    } catch(ignore) {}
    return false;
};

exports.isDirectory = function(path) {
    try {
        return FS.statSync(path).isDirectory();
    } catch(ignore) {}
    return false;
};

/**
 * Filter out non-existent paths.
 *
 * @param {String[]} paths  Paths to filter
 * @return {Promise * String[]}  Existent paths
 */
exports.filterPaths = function(paths) {

    var d = Q.defer(),
        res = [],
        total = paths.length,
        count = 0;

    paths.forEach(function(path, index) {

        PATH.exists(path, function(exists) {

            count++;
            res[index] = exists;

            if (count < total) return;

            d.resolve(paths.filter(function(path, index) {
                return res[index];
            }));

        });

    });

    return d.promise;

};

exports.fsWalkTree = function(root, fileCb, filterCb, ctx) {
    var files = FS.readdirSync(root);
    files.sort();
    while (files.length > 0) {
        var path = PATH.join(root, files.shift());
        if(filterCb && !filterCb.call(ctx, path)) continue;
        fileCb.call(ctx, path);
        if(exports.isDirectory(path)) exports.fsWalkTree(path, fileCb, filterCb, ctx);
    }
};

exports.fsWalkTreeAsync = function(root, fileCb, filterCb, ctx) {
    return QFS.list(root)
        .then(function(files) {
            return Q.all(files.map(function(file) {
                var path = PATH.join(root, file);
                if (!(filterCb && !filterCb.call(ctx, path, file))) {
                    //fileCb.call(ctx, path);
                    return QFS.isDirectory(path)
                        .then(function(isdir) {
                            if (isdir) return exports.fsWalkTreeAsync(path, fileCb, filterCb, ctx);
                        });
                }
            }));
        });
};


exports.getDirs = function(path) {
    try {
        return exports.isDirectory(path)?
            FS.readdirSync(path)
                .filter(function(d) {
                    return !(/^\.svn$/.test(d)) && exports.isDirectory(PATH.join(path, d));
                })
                .sort() :
            [];
    } catch (e) {
        return [];
    }
};

exports.getDirsAsync = function(path) {
    return QFS.list(path).then(function(items) {
        return Q.all(items.map(function(i) {
            return QFS.isDirectory(PATH.join(path, i))
                .then(function(isDir){
                    return {
                        name: i,
                        dir: isDir
                    };
                }
            );
        }))
        .then(function(items) {
                return items
                    .filter(function(item) {
                        return item.dir;
                    })
                    .map(function(item) {
                        return item.name;
                    });
            }
        );
    });
};

exports.getFilesAsync = function(path) {
    return QFS.list(path).then(function(items) {
        return Q.all(items.map(function(i) {
            return QFS.isFile(PATH.join(path, i))
                .then(function(isFile){
                    return {
                        name: i,
                        file: isFile
                    };
                }
            );
        }))
        .then(function(items) {
                return items
                    .filter(function(item) {
                        return item.file;
                    })
                    .map(function(item) {
                        return item.name;
                    });
            }
        );
    });
};

exports.getFiles = function(path) {
    try {
        return exports.isDirectory(path)?
            FS.readdirSync(path)
                .filter(function(f) {
                    return exports.isFile(PATH.join(path, f));
                })
                .sort() :
            [];
    } catch (e) {
        return [];
    }
};

exports.toUpperCaseFirst = function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

/* jshint -W098 */
exports.isEmptyObject = function(obj) {
    for(var i in obj) return false;
    return true;
};
/* jshint +W098 */

exports.isPath = function(str) {
    return PATH.normalize(str).indexOf(PATH.dirSep) !== -1;
};

exports.arrayUnique = function(arr) {
    return arr.reduce(function(prev, cur) {
        if(prev.indexOf(cur) + 1) return prev;
        return prev.concat([cur]);
    }, []);
};

exports.arrayReverse = function(arr) {
    return arr.reduceRight(function(prev, cur) {
        prev.push(cur);
        return prev;
    }, []);
};

exports.stripModuleExt = function(path) {
    var exts = Object.keys(require.extensions).map(function(v) {
        return v.replace(/^\./, '');
    });
    return path.replace(new RegExp('\\.(' + exts.join('|') + ')$'), '');
};

exports.getNodePaths = function() {
    return (process.env.NODE_PATH || '').split(PATH.pathSep);
};

exports.mergeDecls = function mergeDecls(d1, d2) {
    var keys = {};
    d1?
        d1.forEach(function(o) {
            keys[o.name || o] = o;
        }) :
        d1 = [];

    d2.forEach(function(o2) {
        var name = o2.name || o2;
        if (keys.hasOwnProperty(name)) {
            var o1 = keys[name];
            o2.elems && (o1.elems = mergeDecls(o1.elems, o2.elems));
            o2.mods && (o1.mods = mergeDecls(o1.mods, o2.mods));
            o2.vals && (o1.vals = mergeDecls(o1.vals, o2.vals));
            o2.techs && (o1.techs = mergeDecls(o1.techs, o2.techs));
        } else {
            d1.push(o2);
            keys[name] = o2;
        }
    });

    return d1;
};

exports.declForEach = function(decl, cb) {

    var forItemWithMods = function(block, elem) {
            var item = elem || block,
                type = elem? 'elem' : 'block',
                args = elem? [block.name, elem.name] : [block.name];

            // for block and element
            cb(type, args, item);

            // for each modifier
            item.mods && item.mods.forEach(function(mod) {

                // for modifier
                cb(type + '-mod', args.concat(mod.name), mod);

                // for each modifier value
                mod.vals && mod.vals.forEach(function(val, i) {
                    if (!val.name) {
                        val = { name: val };
                        mod.vals[i] = val;
                    }
                    cb(type + '-mod-val', args.concat(mod.name, val.name), val);
                });

            });
        },
        forBlockDecl = function(block) {
            // for block
            forItemWithMods(block);

            // for each block element
            block.elems && block.elems.forEach(function(elem) {
                forItemWithMods(block, elem);
            });
        },
        forBlocksDecl = function(blocks) {
            // for each block in declaration
            blocks.forEach(forBlockDecl);
        };

    decl.name && forBlockDecl(decl);
    decl.blocks && forBlocksDecl(decl.blocks);

};

/**
 * Constructs BEM entity key from entity properties.
 *
 * @param {Object} item  BEM entity object.
 * @param {String} item.block  Block name.
 * @param {String} [item.elem]  Element name.
 * @param {String} [item.mod]  Modifier name.
 * @param {String} [item.val]  Modifier value.
 * @return {String}
 */
exports.bemKey = function(item) {

    var key = '';

    if (item.block) {
        key += item.block;

        item.elem && (key += '__' + item.elem);

        if (item.mod) {
            key += '_' + item.mod;
            item.val && (key += '_' + item.val);
        }
    }

    return key;

};

/**
 * Constructs BEM entity full key from entity properties plus tech name.
 *
 * @param {Object} item  BEM entity object.
 * @param {String} item.block  Block name.
 * @param {String} [item.elem]  Element name.
 * @param {String} [item.mod]  Modifier name.
 * @param {String} [item.val]  Modifier value.
 * @param {String} [item.tech]  Tech name.
 * @return {String}
 */
exports.bemFullKey = function(item) {
    return exports.bemKey(item) + (item.tech? '.' + item.tech : '');
};


var bemItemRe = '([^_.]+)',
    bemKeyRe = new RegExp('^' + bemItemRe +
        '(?:__' + bemItemRe + ')?(?:_' + bemItemRe + '(?:_' + bemItemRe + ')?)?' +
        '(?:\\.([^_]+))?$');

/**
 * Parse BEM-entity key into BEM-entity object.
 *
 * @param {String} key  Key to parse.
 * @return {Object}  BEM-entity object.
 */
exports.bemParseKey = function(key) {

    var m = bemKeyRe.exec(key),
        item = { block: m[1] };

    m[2] && (item.elem = m[2]);
    m[3] && (item.mod = m[3]);
    m[4] && (item.val = m[4]);
    m[5] && (item.tech = m[5]);

    return item;

};


exports.removeFromArray = function(arr, o) {
    var i = arr.indexOf(o);
    return i >= 0 ?
        (arr.splice(i, 1), true) :
        false;
};


exports.getDirsFiles = function(path, dirs, files) {
    return QFS.list(path).then(function(list) {
        return Q.all(list
            .map(function(i) {
                return QFS.isDirectory(PATH.join(path, i))
                    .then(function(isDir) {
                        (isDir ? dirs : files).push(i);
                    });
            }));
    });
};

exports.getDirsFilesSync = function(path, dirs, files) {

    var items = FS.readdirSync(path);

    items.forEach(function(item) {

        if (item[0] === '.') return;

        var stat = FS.lstatSync(path + PATH.dirSep + item),
            file = {
                file: item,
                absPath: path + PATH.dirSep + item,
                lastUpdated: stat.mtime.getTime()
            };

        if (stat.isDirectory()) dirs && dirs.push(file);
        else files && files.push(file);
    });
};

/**
 * Executes specified command with options.
 *
 * @param {String} cmd  Command to execute.
 * @param {Object} options  Options to `child_process.exec()` function.
 * @param {Boolean} resolveWithOutput  Resolve returned promise with command output if true.
 * @return {Promise * String | Undefined}
 */
exports.exec = function(cmd, options, resolveWithOutput) {

    var cp = require('child_process').exec(cmd, options),
        d = Q.defer(),
        output = '';

    cp.on('exit', function(code) {
        if (code === 0) return d.resolve(resolveWithOutput && output ? output : null);
        d.reject(new Error(UTIL.format('%s failed: %s', cmd, output)));
    });

    cp.stderr.on('data', function(data) {
        output += data;
    });

    cp.stdout.on('data', function(data) {
        output += data;
    });

    return d.promise;

};