/**
 * Created by mbakirov on 15.06.15.
 */
/* jshint quotmark: false */

'use strict';

var Q = require('q'),
    path = require('path'),
    fs = require('fs');

module.exports = function() {
    return this
        .title('Clear cache.').helpful()
        .arg().name('block')
            .req()
            .title('Block name')
            .end()
        .opt()
        .name('level').short('l').long('level')
        .def(def)
        .title(['level directory path, default: ',
            !rel? '.' : rel, PATH.dirSep].join(''))
        .val(function(l) {
            return typeof l === 'string'? require('./level').createLevel(l) : l;
        })
        .end()
        .act(function(opts) {

        });

};
