/**
 * Created by mbakirov on 15.06.15.
 */

'use strict';

var Q = require('q'),
    PATH = require('path');

module.exports = function() {

    return this
        .title('Clear caches')
        .helpful()
        .cmd().name('clear').apply(require('./cache/clear')).end();

};
