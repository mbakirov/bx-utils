/**
 * Created by mbakirov on 15.06.15.
 */

'use strict';

var Q = require('q'),
    path = require('path'),
    inquirer = require('inquirer'),
    fs = require('fs'),
    glob = require("glob"),
    rmdir = require('rmdir'),
    BEM = require('bem').api;

module.exports = function() {

    return this
        .title('Operation with blocks tool.')
        .helpful()
        .cmd().name('rm').apply(require('./block/rm')).end()
        //.cmd().name('mv').apply(require('./block/mv')).end()
        .arg().name('block').title('block name, required').req().end()
        .opt().name('level').short('l').long('level').title('level name, default: local/blocks').def('local/blocks').end()
        .opt().name('elem').short('e').long('elem').title('element name').arr().end()
        .opt().name('mod').short('m').long('mod').title('modifier name').arr().end()
        .opt().name('val').short('v').long('val').title('modifier value').arr().end()
        .opt().name('addTech').short('t').long('add-tech').title('add tech').arr().end()
        .opt().name('forceTech').short('T').long('force-tech').title('use only specified tech').arr().end()
        .opt().name('noTech').short('n').long('no-tech').title('exclude tech').arr().end()
        .act(function(opts, args) {
            opts.block = args.block;

            Q.when(BEM.create(opts), function() {
                console.log('Block `%s` created', args.block);
            });
        })
};
